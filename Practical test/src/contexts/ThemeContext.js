import { createContext } from 'react';
import digitalCoasterTheme from '../themes/digitalCoaster';
import zeldaTheme from '../themes/zelda';

const ThemeContext = createContext(digitalCoasterTheme);

export { digitalCoasterTheme, zeldaTheme };
export default ThemeContext;
