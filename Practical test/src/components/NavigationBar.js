import React, { useContext } from 'react';
import ThemeContext from '../contexts/ThemeContext';

const NavigationBar = ({ handleSectionChange, setNavbarMobile, navbarMobile }) => {
  const theme = useContext(ThemeContext);

  const sections = ['about me', 'resume', 'contact', 'resources'];

  return (
    <>
    <nav className={`bg-transparent px-16 sm:text-2xl hidden sm:block`}>
      <ul className="flex justify-between w-full">
        {sections.map(section => (
          <li key={section} className="mx-4">
            <a
              href={`#${section}`}
              onClick={() => handleSectionChange(section)}
              className={`font-light text-${theme.textColor}`}
            >
              {section}
            </a>
          </li>
        ))}
      </ul>
    </nav>
   { navbarMobile && <section style={{backgroundColor: theme.primaryColor, top: 0, left: 0}} className='w-screen h-full absolute flex justify-center items-center text-white text-5xl'>
   <div style={{top: 0, right: 0}} className='absolute' onClick={() => setNavbarMobile()}>
    <p className='p-4'>X</p>
   </div>
      <ul>
        {
          sections.map(section => (
            <li key={section} className="my-8">
              <a href={`#${section}`}
              onClick={() => {handleSectionChange(section); setNavbarMobile()}}
              className="font-light">
                {section}
              </a>
            </li>
          ))
        }
      </ul>
    </section>}
    </>
  );
};

export default NavigationBar;
