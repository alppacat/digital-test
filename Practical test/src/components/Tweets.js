import React, { useContext } from 'react';
import ThemeContext from '../contexts/ThemeContext';
import logo from '../assets/twitter-logo.svg'

const Tweets = () => {
  const theme = useContext(ThemeContext);

  return (
    <div className="sm:w-3/4 p-4 flex items-center flex-col">
      <h2 style={{color: 'red'}} className="text-xl font-light mb-4">
        Latest Tweets
      </h2>
      <div style={{backgroundColor: theme.sidebarColor}} className='w-80 sm:w-96 h-64 rounded-lg flex justify-center flex-col relative items-center'>
        <div style={{top:'-15px'}} className='absolute'>
          <img className='w-12' src={logo} alt="Twitter Logo"/>
        </div>
        <div className='p-12'>
          <p>"Aliquam malesuada consequat erat, et volutpat ex ultrices ut. Praesent a lorem eu metus
          scelerisque placerat."</p>
          <p className='text-zinc-600	text-center'>1 hour ago</p>
        </div>
      </div>
    </div>
  );
};

export default Tweets;
