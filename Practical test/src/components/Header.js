import React, { useContext } from 'react';
import ThemeContext, { zeldaTheme } from '../contexts/ThemeContext';
import NavigationBar from './NavigationBar';

const Header = ({ toggleTheme, handleSection, navbarMobile, setNavbarMobile }) => { 
  const theme = useContext(ThemeContext);

  return (
    <header style={{backgroundColor: theme.primaryColor}} className={`p-4`}>
        <div className='flex justify-end'>
            <label className="relative flex items-center mr-5 cursor-pointer">
                <input type="checkbox" value="" className="sr-only peer" onClick={toggleTheme} checked={theme.name === 'zeldaTheme'} />
                <div className="w-11 h-6 bg-gray-200 rounded-full peer dark:bg-gray-700 peer-focus:ring-4 peer-focus:ring-green-300 dark:peer-focus:ring-green-800 peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-0.5 after:left-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all dark:border-gray-600 peer-checked:bg-green-600"></div>
            </label>
        </div>
      <div className="flex items-center justify-center mb-4">
        <div className="w-24 h-24 rounded-full bg-white">
          <img className="w-full h-full rounded-full object-cover" alt='Profile picture' src={`${theme.profile}`}/>
        </div>
      </div>
      <p className={`text-center mb-2 text-${theme.textColor}`}>{theme.username}</p>
      <div className='flex justify-center items-center'>
        <div className='rounded bg-yellow-400 w-fit p-2 m-3'>
            <p className="text-center inline">{theme.role}</p>
        </div>
      </div>
      <NavigationBar setNavbarMobile={setNavbarMobile} navbarMobile={navbarMobile} handleSectionChange={handleSection} />
      <div style={{top:0}}className="sm:hidden flex justify-start absolute pt-4" onClick={() => setNavbarMobile()}>
        <div className="space-y-2">
          <div className="w-8 h-0.5 bg-white"></div>
          <div className="w-8 h-0.5 bg-white"></div>
          <div className="w-8 h-0.5 bg-white"></div>
        </div>
      </div>
    </header>
  );
};

export default Header;
