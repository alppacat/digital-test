import React, { useContext } from 'react';
import ThemeContext from '../contexts/ThemeContext';

const AboutMe = () => {
    const theme = useContext(ThemeContext);

  return (
    <div id="about">
      <h2 style={{color: theme.primaryColor}} className="text-xl font-light mb-4">About me</h2>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed volutpat diam nec tortor
        condimentum, vel ullamcorper lorem rutrum. Vestibulum mollis ipsum sit amet mauris
        suscipit, ut elementum purus venenatis. Suspendisse potenti. Vivamus ut turpis urna. Cras
        sed mauris eget mauris feugiat dapibus. Sed nec tellus eu tortor lobortis semper. In auctor
        sagittis lectus, eu feugiat mauris fermentum a. Nullam ut nibh ante. Ut interdum pharetra
        ultrices. Nulla ut felis eget enim pellentesque pharetra.
      </p>
      <p>
        Aliquam malesuada consequat erat, et volutpat ex ultrices ut. Praesent a lorem eu metus
        scelerisque placerat. Fusce ut felis at odio fringilla dignissim. Quisque sit amet elit id
        nulla aliquam suscipit. Fusce maximus feugiat nunc vitae suscipit. Nullam ac interdum mi.
        Pellentesque tincidunt fermentum risus, vel pharetra enim vehicula id. In hac habitasse
        platea dictumst. In dapibus consectetur ligula at dapibus. Suspendisse auctor ligula id
        lectus viverra gravida. Nullam posuere posuere est, vel volutpat leo laoreet eget. Ut eu
        vulputate ex, ac condimentum ex.
      </p>
    </div>
  );
};

export default AboutMe;
