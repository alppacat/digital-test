import React from 'react';
import About from './Aboutme'
import Resume from './Resume'
import Contact from './Contact'
import Resources from './Resources'
import Tweets from './Tweets';

const Content = ({ selectedSection }) => {
  let sectionContent = null;

  switch (selectedSection) {
    case 'about me':
      sectionContent = (
        <About />
      );
      break;
    case 'resume':
      sectionContent = (
        <Resume />
      );
      break;
    case 'contact':
      sectionContent = (
       <Contact />
      );
      break;
    case 'resources':
      sectionContent = (
       <Resources />
      );
      break;
    default:
      sectionContent = (
        <div>
          <h2>Invalid Section</h2>
          <p>This section does not exist.</p>
        </div>
      );
  }

  return (
    <>
      <div className={selectedSection === 'about me' ? `sm:w-4/5 p-4 p4` : 'p-4 p4'}>
      {sectionContent}
    </div>
    {selectedSection === 'about me' &&
      <Tweets />
    }
    </>

  );
};

export default Content;
