import image from '../assets/profile-pic.jpeg'

const digitalCoasterTheme = {
    name: 'digitalCoasterTheme',
    username: 'John Doe',
    role: 'senior web developer',
    primaryColor: '#212A97',
    textColor: 'white',
    sidebarColor: 'rgb(165 180 252)',
    jobDescription: 'black',
    profile: image
  };
  
  export default digitalCoasterTheme;
  