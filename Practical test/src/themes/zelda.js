import image from '../assets/zelda-pic.jpeg'
const zeldaTheme = {
    name: 'zeldaTheme',
    username: 'Zelda',
    role: 'hyrule princess',
    primaryColor: '#276267',
    textColor: 'white',
    sidebarColor: '#56a3a0',
    profile: image
  };
  
  export default zeldaTheme;
  