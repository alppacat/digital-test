import React, { useState } from 'react';
import Header from './components/Header';
import Content from './components/Content';
import ThemeContext, { digitalCoasterTheme, zeldaTheme } from './contexts/ThemeContext';

const App = () => {
  const [selectedTheme, setSelectedTheme] = useState(digitalCoasterTheme);
  const [selectedSection, setSelectedSection] = useState('about me');
  const [navbarMobile, setNavbarMobile] = useState(false)

  const handleNavBar = () => setNavbarMobile(!navbarMobile)
  const toggleTheme = () => {
    setSelectedTheme(prevTheme => (prevTheme === digitalCoasterTheme ? zeldaTheme : digitalCoasterTheme));
  };

  const handleSectionChange = section => {
    setSelectedSection(section);
  };

  return (
    <ThemeContext.Provider value={selectedTheme}>
      <div className={`app ${selectedTheme.primaryColor} ${selectedTheme.textColor}`}>
        <Header navbarMobile={navbarMobile} setNavbarMobile={handleNavBar} toggleTheme={toggleTheme} handleSection={handleSectionChange}/>
        <div className="flex flex-col p-6 sm:flex-row">
          <Content selectedSection={selectedSection} handleSectionChange={handleSectionChange} />
        </div>
      </div>
    </ThemeContext.Provider>
  );
};

export default App;
