## Project Stack
- React: A JavaScript library for building user interfaces.
- CSS: Cascading Style Sheets for styling the components and layout.
- HTML: Hypertext Markup Language for creating the structure of the web pages.
- Tailwind CSS: A utility-first CSS framework used for rapid UI development.

<img src="src/assets/mobile.png" alt="Image Description" style="width:300px;" />
## Project Structure

The project structure is organized as follows:

- src
  - components
    - AboutMe.js
    - Contact.js
    - Content.js
    - Header.js
    - NavigationBar.js
    - Tweets.js
    - Resources.js
  - assets
    - images
      - profile-image.jpg
  - contexts
    - ThemeContext.js
    - themes
      - digitalCoasterTheme.js
      - zeldaTheme.js
  - App.js
  - index.js


- The `components` folder contains all the individual components used in the project. Each component file represents a specific section of the website, including `AboutMe`, `Contact`, `Content`, `Header`, `NavigationBar`, `Tweets`, and `Resources`.

- The `assets` folder contains any static assets used in the project, such as images. In this example, it includes a `profile-image.jpg`.

- The `contexts` folder includes the context files used in the project. It contains `ThemeContext.js`, which defines the theme context for the website. The `themes` folder within `contexts` includes separate theme files, such as `digitalCoasterTheme.js` and `zeldaTheme.js`, that define different theme options for the website.

- The `App.js` file is the entry point of the application, where the main structure and components are rendered. The `index.js` file is responsible for rendering the app into the root HTML element.

## How to Run the React App

To run the React app, follow these steps:

1. Clone the repository or download the project files.

2. Open a terminal or command prompt and navigate to the project directory.

3. Install the project dependencies by running the following command:

   ```bash
   npm install
4. Once the installation is complete, start the development server by running the following command:

    ```bash
    npm start
5. The app should now be running on localhost:3000 in your web browser. You can access the website and explore the different components and themes.