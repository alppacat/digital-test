<img src="assets/htmlcss.png" alt="Image Description" style="width:300px;" />

# HTML and CSS

The project is based on the following GIF: [GIF Animation](https://i.gifer.com/7rtw.gif).

## Technologies Used

The project is developed using the following technologies:

- HTML
- CSS

## YouTube Video Modal

To ensure the video playback is paused when the video modal is closed, a small JavaScript script is used. The script listens for the modal close event and triggers the pause action on the video element.

Although JavaScript is used for this specific functionality, it is important to note that the primary animation and the core functionality of the website are achieved solely through HTML and CSS.

## Responsiveness

The website is designed to be fully responsive, adapting to different screen sizes and devices. The layout and styling are optimized to provide a seamless experience across various platforms.