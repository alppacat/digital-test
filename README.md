# Project Readme

This repository contains various exercises and projects using different technologies. Each folder includes a README file explaining the details and stack used to solve it.

## Folder Structure

- **Practial test**: Single page application made with React and Tailwind CSS.
- **Use HTML & CSS**: Page made with HTML and CSS.
- **Use Laravel**: Laravel project.
- **Use JavaScript**: JavaScript exercise.
- **Use PHP**: PHP and JavaScript exercises.
