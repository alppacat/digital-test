/** @type {import('tailwindcss').Config} */
module.exports = {
  theme: {
    extend: {
      colors: {
        primary: '#276267',
        secondary: '#56a3a0',
        tertiary: '#b7d7ce',
        accent: '#8f8c3f',
        dark: '#564f2d',
      },
    },
  },
  content: [
    './storage/framework/views/*.php',
    './resources/**/*.blade.php',
    './resources/**/*.js',
    './resources/**/*.vue',
],
}