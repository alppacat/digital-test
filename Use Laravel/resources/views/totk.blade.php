<!DOCTYPE html>
<html>
<head>
    <title>Landing Page - Tears of the Kingdom</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
</head>
<body class="bg-primary bg-pattern flex flex-col min-h-screen">
    <header class="text-white p-4 bg-[#E60013]">
        <a href="https://www.nintendo.com" target="_blank">
            <img src="{{ asset('assets/nintendo-logo.svg') }}" alt="Nintendo" width="97" height="24">
        </a>
    </header>
    <section class="hero bg-tertiary bg-cover bg-center flex items-start flex-grow w-full hero-image">
    </section>
    <main class="container mx-auto py-8 px-4 sm:px-6 lg:px-8 flex-grow">
        <section class="text-center mt-2 mb-5">
            <h1 class="text-4xl md:text-5xl lg:text-6xl font-bold text-white mb-4 md:mb-8 inline">Discover</h1>
            <span class="text-4xl md:text-5xl lg:text-6xl font-bold text-white mb-4 md:mb-8 italic">THE LEGEND</span>
        </section>
        <section>
            <div class="carousel">
                <img class="carousel-slide active" src="{{ asset('assets/skies.jpeg') }}" alt="Sky Islands">
                <img class="carousel-slide" src="{{ asset('assets/surface.jpeg') }}" alt="Surface">
                <img class="carousel-slide" src="{{ asset('assets/depths.jpeg') }}" alt="Depths">
            </div>
        </section>
    </main>
    <!-- Footer -->
    <footer class="bg-dark text-center text-white p-4 bg-pattern-footer">
        <p class="text-sm md:text-base">El juego, la consola y las figuras amiibo se venden por separado.</p>
        <p class="text-sm md:text-base">© Nintendo.</p>
    </footer>
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        // Image slider
        document.addEventListener('DOMContentLoaded', function() {
            const carousel = document.querySelector('.carousel');
            const slides = carousel.querySelectorAll('.carousel-slide');
            let currentSlide = 0;

            function showSlide(index) {
                slides.forEach((slide, i) => {
                    if (i === index) {
                        slide.classList.add('active');
                    } else {
                        slide.classList.remove('active');
                    }
                });
            }

            function nextSlide() {
                currentSlide = (currentSlide + 1) % slides.length;
                showSlide(currentSlide);
            }

            setInterval(nextSlide, 3000);
        });
    </script>
</body>
</html>
