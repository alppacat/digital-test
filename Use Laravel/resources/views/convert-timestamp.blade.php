<!DOCTYPE html>
<html class="h-screen w-screen">
<head>
    <title>Converted Timestamp</title>
    <link rel="stylesheet" type="text/css" href="{{ url('css/app.css') }}">
</head>
<body class="h-full bg-[#F5F5F7]">
    <main class="flex flex-col h-full w-full items-center">
        <div class="flex flex-col h-full justify-center w-fit	">
            <h1 class="font-sans text-6xl  text-[#3e3d3d]">Converted Timestamp:</h1>
            <p class="font-sans text-3xl text-center mt-6 text-[#737272]">{{ $convertedTimestamp }}</p>
        </div>
    </main>
</body>
</html>
