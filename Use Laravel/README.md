<img src="public/assets/convert-stime.png" alt="Image Description" style="width:300px;" />
<img src="public/assets/totk.png" alt="Image Description" style="width:300px;" />

## Stack Used

- Laravel
- PHP
- JavaScript
- Tailwind CSS

## Routes

- `/`: The default route that displays the Laravel 8.x documentation.

- `/convert-timestamp`: This route demonstrates the usage of the `HomeController` and `Home` model. It converts the provided timestamp "2021-03-17 02:31:01" to the date format "Y-m-s" and returns it as a string.

- `/totk`: This route showcases a mini landing page for "Zelda: Tears of the Kingdom".

## Convert Timestamp

To convert a timestamp to the desired date format, the following steps have been implemented:

1. Create the `HomeController` controller.

2. Create the `Home` model.

3. Inside the `Home` model, add a function named `convertTimestamp()` that accepts a timestamp as input. The function converts the timestamp to the "Y-m-s" date format and returns it as a string.

4. In the `HomeController`, call the `convertTimestamp()` function from the `Home` model, passing the timestamp "2021-03-17 02:31:01" as the argument.

5. Display the converted timestamp in one of your routes.

## Tears of the Kingdom (TOTK)

The `/totk` route presents a mini landing page for the game "Zelda: Tears of the Kingdom". 



