<?php
// app/Http/Controllers/HomeController.php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Game;

class HomeController extends Controller
{
    public function index()
    {
        // Retrieve data from the Game model if needed
        $gameData = "";

        // Pass data to the view
        return view('landing', compact('gameData'));
    }
}
