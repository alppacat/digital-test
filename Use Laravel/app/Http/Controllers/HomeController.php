<?php

namespace App\Http\Controllers;

use App\Models\Home;

class HomeController extends Controller
{
    public function convertTimestamp()
    {
        $timestamp = '2021-03-17 02:31:01';
        $convertedTimestamp = Home::convertToDateFormat($timestamp);
        
        return view('convert-timestamp', compact('convertedTimestamp'));
    }
}
