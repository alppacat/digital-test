<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Home extends Model
{
    public static function convertToDateFormat($timestamp)
    {
        return date('Y-m-d', strtotime($timestamp));
    }
}

