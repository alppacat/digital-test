<?php
function extractEachKth($inputArray, $k) {
    $result = array();
    $count = 0;

    foreach ($inputArray as $element) {
        $count++;
        if ($count % $k !== 0) {
            $result[] = $element;
        }
    }

    return $result;
}

$inputArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
$k = 5;

$output = extractEachKth($inputArray, $k);
print_r($output);

?>