# extractEachKth

Given an array of integers, this function removes every k-th element from the array.

## Example

```php
inputArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
k = 3
output = extractEachKth(inputArray, k)
print(output)  # [1, 2, 4, 5, 7, 8, 10]
```

## Input

- `inputArray` (array of integers): An array of integers.
  - Guaranteed constraints: 
    - 5 ≤ inputArray.length ≤ 15
    - -20 ≤ inputArray[i] ≤ 20.

- `k` (integer): The interval for removing elements.
  - Guaranteed constraints: 1 ≤ k ≤ 10.

## Output

- Returns an array of integers: The `inputArray` with elements at positions k - 1, 2k - 1, 3k - 1, and so on, removed.