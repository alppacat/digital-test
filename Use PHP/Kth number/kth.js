function extractEachKth(inputArray, k) {
    let result = [];
  
    for (let i = 0; i < inputArray.length; i++) {
      if ((i + 1) % k !== 0) {
        result.push(inputArray[i]);
      }
    }
  
    return result;
  }
const inputArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
const k = 3;

const output = extractEachKth(inputArray, k);
console.log(output);