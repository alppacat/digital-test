<?php
function validTime($time) {
    list($hours, $minutes) = explode(':', $time);
    return $hours >= 0 && $hours <= 23 && $minutes >= 0 && $minutes < 60;
}
$first = "13:58";
$second = "25:51";
$third = "02:76";

$result1 = validTime($first); 
$result2 = validTime($second); 
$result3= validTime($third);

echo $result1 ? "true" : "false";
echo $result2 ? "true" : "false";
echo $result3 ? "true" : "false";
?>
