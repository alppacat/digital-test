function validTime(time) {
    const [hours, minutes] = time.split(':').map(Number);
    return hours >= 0 && hours <= 23 && minutes >= 0 && minutes < 60;
  }

const first = "13:58";
const second = "25:51";
const third = "02:76";
  
console.log(validTime(first));
console.log(validTime(second));
console.log(validTime(third));