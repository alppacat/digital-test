# validTime

This function checks if the given string is a correct time representation of the 24-hour clock.

## Example

```php
time = "13:58"
output = validTime(time)
print(output)  # True

time = "25:51"
output = validTime(time)
print(output)  # False

time = "02:76"
output = validTime(time)
print(output)  # False
```

## Input

- `time` (string): A string representing time in HH:MM format. It is guaranteed that the first two characters, as well as the last two characters, are digits.

## Output

- Returns a boolean: `True` if the given `time` is a valid representation of the 24-hour clock, `False` otherwise.