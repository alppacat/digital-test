function isLucky(n) {
    const ticketNumber = String(n);
    const ticketLength = ticketNumber.length;
    const half = ticketLength / 2;
    const sum1 = ticketNumber.slice(0, half).split('').reduce((a, b) => +a + +b);
    const sum2 = ticketNumber.slice(half).split('').reduce((a, b) => +a + +b);
  
    return sum1 === sum2;
  }
  
  console.log(isLucky(1230)); 
  console.log(isLucky(239017));